#!/usr/bin/env python3
import automation
import json
import openstack
import os
import threading
import tkinter as tk
import tkinter.ttk as ttk


class LoginFrame(tk.Frame):
    def __init__(self, window):
        super().__init__(window)
        self.window = window

        self.inputs = {
            "university_username": tk.StringVar(),
            "nrec_username": tk.StringVar(),
            "project_name": tk.StringVar(),
            "api_key": tk.StringVar(),
            "region": tk.StringVar()
        }

        if os.path.exists("keystoneauth.json"):
            with open("keystoneauth.json", "r") as f:
                keystone = json.load(f)  # Gets login details if they have previously been entered
            for key, value in self.inputs.items():
                value.set(keystone[key])

        tk.Label(self, text="University Username").grid(row=0, column=0, pady=2)
        tk.Entry(self, textvariable=self.inputs["university_username"]).grid(row=0, column=1, pady=2)

        tk.Label(self, text="NREC Username").grid(row=1, column=0, pady=2)
        tk.Entry(self, textvariable=self.inputs["nrec_username"]).grid(row=1, column=1, pady=2)

        tk.Label(self, text="Project Name").grid(row=2, column=0, pady=2)
        tk.Entry(self, textvariable=self.inputs["project_name"]).grid(row=2, column=1, pady=2)

        tk.Label(self, text="API Key").grid(row=3, column=0, pady=2)
        tk.Entry(self, textvariable=self.inputs["api_key"], show="*").grid(row=3, column=1, pady=2)

        tk.Label(self, text="Location").grid(row=4, column=0, pady=2)
        tk.OptionMenu(self, self.inputs["region"], "bgo", "osl").grid(row=4, column=1, pady=2, sticky="we")

        tk.Button(self, text="Continue", command=self.submit_info).grid(row=5, column=0, columnspan=2, pady=2)

    def submit_info(self):
        credentials = [info.get() for info in self.inputs.values()]
        if all(credentials):
            new_keystone = {key: value.get() for (key, value) in self.inputs.items()}
            with open("keystoneauth.json", "w") as f:
                json.dump(new_keystone, f)
            automation.set_env(*credentials[1:])
            ServerSetupFrame(self.window, self.inputs["university_username"].get()).pack(padx=(10, 10), pady=(10, 10))
            self.pack_forget()


class ServerSetupFrame(tk.Frame):
    def __init__(self, window, uni_name: str):
        super().__init__(window)
        self.window = window
        self.conn = openstack.connect()
        self.server_list = None
        threading.Thread(target=self.get_servers).start()

        self.uni_name = uni_name
        self.keyname = tk.StringVar()
        self.key_passphrase = tk.StringVar()
        self.servername = tk.StringVar()
        self.source = tk.StringVar()
        self.flavor = tk.StringVar()

        tk.Label(self, text="Key Name").grid(row=0, column=0, pady=2)
        tk.Entry(self, textvariable=self.keyname).grid(row=0, column=1, pady=2)

        tk.Label(self, text="Key Passphrase").grid(row=1, column=0, pady=2)
        tk.Entry(self, textvariable=self.key_passphrase, show="*").grid(row=1, column=1, pady=2)

        tk.Label(self, text="Server Name").grid(row=2, column=0, pady=2)
        tk.Entry(self, textvariable=self.servername).grid(row=2, column=1, pady=2)

        tk.Label(self, text="Source").grid(row=3, column=0, pady=2)
        self.source_menu = tk.OptionMenu(self, self.source, "Wait for sources to load")
        self.source_menu.grid(row=3, column=1, pady=2, sticky="we")
        threading.Thread(target=self.source_menu_options).start()

        tk.Label(self, text="Flavor").grid(row=4, column=0, pady=2)
        self.flavor_menu = tk.Label(self, text="Choose a source first")
        self.flavor_menu.grid(row=4, column=1, pady=2)

        tk.Button(self, text="Continue", command=self.submit_info).grid(row=5, column=0, columnspan=2, pady=2)

    def get_servers(self):
        self.server_list = self.conn.list_servers()

    def source_menu_options(self):
        sources = [image["name"] for image in self.conn.list_images()
                   if image["status"] == "active"
                   and "windows" not in image["name"].lower()]
        self.source_menu.grid_forget()
        tk.OptionMenu(self, self.source, *sources, command=self.flavor_menu_options).grid(row=3, column=1, pady=2, sticky="we")

    def flavor_menu_options(self, source):
        self.flavor_menu.grid_forget()
        self.flavor_menu = tk.Label(self, text="Flavors are loading")
        self.flavor_menu.grid(row=4, column=1, pady=2)
        threading.Thread(target=self.add_flavor_menu, args=(source, )).start()

    def add_flavor_menu(self, source):
        flavors = get_possible_flavors(self.conn, source)
        self.flavor_menu.grid_forget()
        self.flavor = tk.StringVar()
        self.flavor_menu = tk.OptionMenu(self, self.flavor, *flavors)
        self.flavor_menu.grid(row=4, column=1, pady=2, sticky="we")

    def submit_info(self):
        server_info = [info.get() for info in
                       (self.keyname, self.servername, self.source, self.flavor)]
        if all(server_info):
            tk.Label(self, text="Server is being created").grid(row=6, column=0, pady=2)
            progress = ttk.Progressbar(self, length=100)
            progress.grid(row=6, column=1, pady=2)
            threading.Thread(target=self.create_instance, args=(progress, )).start()

    def create_instance(self, progress):
        automation.create_keypair(self.conn, self.keyname.get(), self.key_passphrase.get())
        progress["value"] = 25
        automation.create_security_group(self.conn, os.environ["OS_REGION_NAME"])
        progress["value"] = 50
        server = self.conn.create_server(
            self.servername.get(),
            image=self.source.get(),
            flavor=self.flavor.get(),
            security_groups=[f"ICMP_and_SSH_{os.environ['OS_REGION_NAME']}", "default"],
            key_name=self.keyname.get(),
            nics=[{"net-name": "IPv6"}]
        )
        progress["value"] = 75
        username, ipv6 = automation.get_login_info(
            self.conn,
            server["id"],
        )
        automation.create_ssh_config(ipv6, self.uni_name, self.keyname.get())
        progress["value"] = 100
        automation.save_login_info(server, f"ssh {username}@{ipv6}")
        self.pack_forget()
        DisplayServerFrame(self.window, f"ssh {username}@{ipv6}").pack(padx=(10, 10), pady=(10, 10))


class DisplayServerFrame(tk.Frame):
    def __init__(self, window, ssh_command):
        super().__init__(window)
        self.window = window

        tk.Label(self, text=ssh_command).grid(row=0, column=0, pady=2)
        tk.Button(self, text="Copy", command=lambda: self.copy(ssh_command)).grid(row=0, column=1, pady=2)

    def copy(self, text: str):
        self.window.clipboard_clear()
        self.window.clipboard_append(text)
        self.window.update()


def get_user_limits(conn) -> tuple[int, int, int]:  # Returns the resources a user has left unless at least one is at 0
    compute_limits = conn.get_compute_limits()
    instances_left = compute_limits["maxTotalInstances"] - compute_limits["totalInstancesUsed"]
    cores_left = compute_limits["maxTotalCores"] - compute_limits["totalCoresUsed"]
    ram_left = compute_limits["maxTotalRAMSize"] - compute_limits["totalRAMUsed"]

    # Checks if the user is out of resources
    for resource, value in zip(
            ("instances", "cores", "ram"),
            (instances_left, cores_left, ram_left)
    ):
        if value <= 0:
            raise automation.NotEnoughResources(f"There are no {resource} left.")
    return instances_left, cores_left, ram_left


def get_source_minimums(conn, source: str) -> tuple[int, int]:
    image = conn.get_image(source)
    return image["min_ram"], image["min_disk"]


def get_possible_flavors(conn, source: str) -> list[str]:
    instances_left, cores_left, ram_left = get_user_limits(conn)    # Resources the user has left
    source_ram, source_storage = get_source_minimums(conn, source)  # Resource demand for a certain source

    possible_flavors = []
    # Checks which flavors are available to the user and can support a certain source
    for flavor in conn.list_flavors():
        flavor_cores = flavor["vcpus"]
        flavor_ram = flavor["ram"]
        flavor_storage = flavor["disk"]

        if flavor_cores > cores_left or flavor_ram > ram_left:
            continue
        if flavor_ram < source_ram or flavor_storage < source_storage:
            continue

        possible_flavors.append(flavor["name"])

    if not possible_flavors:
        raise automation.NotEnoughResources("No flavor meets the resource demand.")
    return possible_flavors


def main():
    app = tk.Tk()
    # app.geometry("700x200")
    app.title("NREC")
    LoginFrame(app).pack(padx=(10, 10), pady=(10, 10))
    app.mainloop()


if __name__ == "__main__":
    main()
