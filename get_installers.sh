#!/usr/bin/env bash

installanaconda='#!/bin/bash

conda_version=Anaconda3-2022.05-Linux-x86_64.sh

sudo yum install wget -y
wget https://repo.anaconda.com/archive/"$conda_version"
bash "$conda_version" -b
eval "$(~/anaconda3/bin/conda shell.bash hook)"
conda init'

echo "$installanaconda" > installanaconda.sh
chmod 777 installanaconda.sh


installminiconda='#!/bin/bash

sudo yum install wget -y
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -b
eval "$(~/miniconda3/bin/conda shell.bash hook)"
conda init
pip install jupyter
echo "==> For changes to take effect, close and re-open your current shell. <=="'

echo "$installminiconda" > installminiconda.sh

chmod 777 installminiconda.sh


installr='#!/bin/bash

conda install -c conda-forge r-irkernel -y'

echo "$installr" > installr.sh

chmod 777 installr.sh


installjulia='
#!/bin/bash

conda install -c conda-forge julia -y
julia -e "using Pkg; Pkg.add(\"IJulia\")"
'

echo "$installjulia" > installjulia.sh

chmod 777 installjulia.sh


installeessi='#!/bin/bash

case $(whoami) in
  centos|rocky|almalinux)
    sudo yum install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
    sudo yum install -y cvmfs

    sudo yum install -y https://github.com/EESSI/filesystem-layer/releases/download/latest/cvmfs-config-eessi-latest.noarch.rpm
    ;;
  debian|ubuntu)
    wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
    sudo dpkg -i cvmfs-release-latest_all.deb
    rm -f cvmfs-release-latest_all.deb
    sudo apt-get -y update
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install cvmfs

    wget https://github.com/EESSI/filesystem-layer/releases/download/latest/cvmfs-config-eessi_latest_all.deb
    sudo dpkg -i cvmfs-config-eessi_latest_all.deb
    rm -f cvmfs-config-eessi_latest_all.deb
    ;;
  fedora)
    sudo dnf install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-config/cvmfs-config-default-latest.noarch.rpm
    sudo dnf install -y https://github.com/EESSI/filesystem-layer/releases/download/latest/cvmfs-config-eessi-latest.noarch.rpm
    sudo dnf install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-2.9.4/cvmfs-2.9.4-1.fc34.x86_64.rpm
    ;;
esac
sudo bash -c "echo CVMFS_CLIENT_PROFILE=single > /etc/cvmfs/default.local"

sudo cvmfs_config setup'

echo "$installeessi" > installeessi.sh

chmod 777 installeessi.sh
