#!/usr/bin/env python3
import argparse
import csv
import json
import keystoneauth1
import openstack
import os
import subprocess
import sys
from time import sleep


class NotEnoughResources(Exception):
    pass


def get_arguments() -> tuple:
    if os.path.exists("keystoneauth.json"):
        with open("keystoneauth.json", "r") as f:
            keystone = json.load(f)     # Gets login details if they have previously been entered
    else:
        keystone = {}

    parser = argparse.ArgumentParser(description="Automatically create an nrec server. ")
    parser.add_argument("-uu", "--university_username", help="University Username")
    parser.add_argument("-nu", "--nrec_username", help="NREC_Username")
    parser.add_argument("-pn", "--project_name", help="Project Name")
    parser.add_argument("-a", "--api_key", help="NREC API key")
    parser.add_argument("-r", "--region", help="Region", choices=["bgo", "osl"])
    parser.add_argument("-k", "--key", default="nrec", help="Key")
    parser.add_argument("-kp", "--key_passphrase", default="", help="Key Passphrase")
    parser.add_argument("-sn", "--server_name", default="myserver", help="Server Name")
    parser.add_argument("-s", "--source", default="GOLD Ubuntu 22.04 LTS", help="Source")
    parser.add_argument("-f", "--flavor", default="m1.small", help="Flavor")
    parser.add_argument("--list_sources", default=False, const=True, action="store_const", help="List sources")
    parser.add_argument("--list_flavors", default=False, const=True, action="store_const", help="List flavors")

    args = vars(parser.parse_args())
    for key, value in list(args.items())[:5]:
        if value:
            keystone[key] = value       # If a value has been passed as an argument it will be used
        else:
            args[key] = keystone[key]   # If no value was passed as an argument the value in the keystone will be used
    open("keystoneauth.json", "w").close()
    os.chmod("keystoneauth.json", 0o600)
    with open("keystoneauth.json", "w") as f:
        json.dump(keystone, f)          # Update the keystone with the values that ended up being used
    return tuple(args.values())


def set_env(username: str, project_name: str, password: str, region: str) -> None:
    env_variables = {
        "OS_USERNAME": username,
        "OS_PROJECT_NAME": project_name,
        "OS_PASSWORD": password,
        "OS_AUTH_URL": "https://api.nrec.no:5000/v3",
        "OS_IDENTITY_API_VERSION": "3",
        "OS_USER_DOMAIN_NAME": "dataporten",
        "OS_PROJECT_DOMAIN_NAME": "dataporten",
        "OS_REGION_NAME": region,
        "OS_INTERFACE": "public",
        "OS_NO_CACHE": "1"
    }

    for key, value in env_variables.items():
        os.environ[key] = value


def get_user_limits(conn) -> tuple[int, int, int]:  # Returns the resources a user has left unless at least one is at 0
    compute_limits = conn.get_compute_limits()
    instances_left = compute_limits["maxTotalInstances"] - compute_limits["totalInstancesUsed"]
    cores_left = compute_limits["maxTotalCores"] - compute_limits["totalCoresUsed"]
    ram_left = compute_limits["maxTotalRAMSize"] - compute_limits["totalRAMUsed"]
    return instances_left, cores_left, ram_left


def print_possible_flavors(conn) -> None:
    instances_left, cores_left, ram_left = get_user_limits(conn)    # Resources the user has left

    # Checks if the user is out of resources
    for resource, value in zip(
            ("instances", "cores", "ram"),
            (instances_left, cores_left, ram_left)
    ):
        if value <= 0:
            print(f"No flavors available because there are no {resource} left.")
            return

    # Checks which flavors are available to the user
    flavors = []
    for flavor in conn.list_flavors():
        flavor_name = flavor["name"]
        flavor_cores = flavor["vcpus"]
        flavor_ram = flavor["ram"]
        flavor_disk = flavor["disk"]

        if flavor_cores <= cores_left and flavor_ram <= ram_left:
            flavors.append((flavor_name, flavor_cores, flavor_ram, flavor_disk))
    flavors.sort(key=lambda x: x[2])

    print(f"{'VCPUS':>20}{'RAM':>10}{'DISK':>10}")
    for flavor in flavors:
        print(f"{flavor[0]:<10}{flavor[1]:>10}{f'{flavor[2]} MB':>10}{f'{flavor[3]} GB':>10}")


def list_options(conn, list_sources: bool, list_flavors: bool) -> bool:
    have_listed = False
    if list_sources:
        print("Sources: ")
        sources = [image["name"] for image in conn.list_images()
                   if image["status"] == "active"
                   and "windows" not in image["name"].lower()]
        print(*sources, sep="\n", end="\n\n")
        have_listed = True
    if list_flavors:
        print("Flavors: ")
        print_possible_flavors(conn)
        print()
        have_listed = True
    return have_listed


def create_keypair(conn, key_name: str, key_passphrase: str) -> None:
    key_loc = f"{os.path.expanduser('~')}/.ssh/{key_name}"

    """
    If keys exist locally but not in NREC -> Add existing keys to NREC
    If keys exist locally and in NREC -> No action taken
    If keys does not exist locally but exist in NREC -> Make and save new keypair locally and overwrite the one at NREC
    If keys does not exist both locally and in NREC -> Make and save new keypair locally and add to NREC
    
    When comparisons are made, key names are only considered, trying to use an existing key with a new passphrase will
    not change the passphrase
    """
    if os.path.exists(key_loc) and os.path.exists(f"{key_loc}.pub"):
        if not conn.get_keypair(key_name):
            with open(f"{key_loc}.pub") as f:
                conn.create_keypair(key_name, public_key=f.read())
        return
    elif conn.get_keypair(key_name):
        conn.delete_keypair(key_name)

    extension = ".exe" if sys.platform == "win32" else ""   # To make sure ssh-keygen works both in Windows and UNIX
    subprocess.run([f"ssh-keygen{extension}", "-b", "4096", "-t", "rsa", "-f", key_loc, "-N", key_passphrase, "-q"])

    with open(f"{key_loc}.pub", "r") as f:  # Read the newly created public key and add to NREC
        conn.create_keypair(key_name, public_key=f.read())


def create_security_group(conn, region: str) -> None:
    group_name = f"ICMP_and_SSH_{region}"
    if conn.get_security_group(group_name):     # Do nothing if a security group with the same name already exists
        return
    conn.create_security_group(group_name, "Allow incoming ICMP and SSH")
    if region == "bgo":     # Bergen IP addresses
        ipv4 = "129.177.0.0/16"
        ipv6 = "2001:700:200::/48"
    else:                   # Oslo IP addresses
        ipv4 = "129.240.0.0/16"
        ipv6 = "2001:700:100::/41"

    conn.create_security_group_rule(
        group_name,
        protocol="icmp",
        remote_ip_prefix=ipv4,
        ethertype="IPv4"
    )
    conn.create_security_group_rule(
        group_name,
        protocol="tcp",
        remote_ip_prefix=ipv4,
        ethertype="IPv4",
        port_range_max="22",
        port_range_min="22"
    )
    conn.create_security_group_rule(
        group_name,
        protocol="ipv6-icmp",
        remote_ip_prefix=ipv6,
        ethertype="IPv6"
    )
    conn.create_security_group_rule(
        group_name,
        protocol="tcp",
        remote_ip_prefix=ipv6,
        ethertype="IPv6",
        port_range_max="22",
        port_range_min="22"
    )


def create_ssh_config(ipv6: str, university_username: str, key: str) -> None:
    universities = {
        "bgo": "uib",
        "osl": "uio"
    }
    region = os.environ["OS_REGION_NAME"]
    university = universities[region]
    ssh_loc = f"{os.path.expanduser('~')}/.ssh"

    server_config = f"""Host {ipv6}
    ProxyJump {university_username}@login.{university}.no
    IdentityFile ~/.ssh/{key}
    Localforward 8888 localhost:8888\n\n"""

    if sys.platform == "win32":     # Windows does not support ssh multiplexing
        proxy_config = f"""Host login.{university}.no
    User {university_username}\n\n"""
    else:
        proxy_config = f"""Host login.{university}.no
    User {university_username}
    ControlPath ~/.ssh/controlmasters/%r@%h:%p
    ControlMaster auto
    ControlPersist 10m\n\n"""

        if not os.path.exists(f"{ssh_loc}/controlmasters"):
            os.mkdir(f"{ssh_loc}/controlmasters", mode=0o700)

    if os.path.exists(f"{ssh_loc}/config"):
        with open(f"{ssh_loc}/config", "r") as f:
            existing_config = f.read()
    else:
        existing_config = ""

    # Makes sure we always connect through proxy, the right key is used,
    # and users only need to authenticate at the start of a session
    with open(f"{ssh_loc}/config", "a") as f:
        if server_config not in existing_config:
            f.write(server_config)
        if proxy_config not in existing_config:
            f.write(proxy_config)

    os.chmod(f"{ssh_loc}/config", 0o600)


def get_by_partial_name(table: dict[str, str], name: str) -> str:
    for key, value in table.items():    # If a part of name is a dictionary key the corresponding value will be returned
        if key in name.lower():
            return value


def get_login_info(conn, server_id: str) -> tuple[str, str]:
    user_names = {
        "alma": "almalinux",
        "centos": "centos",
        "debian": "debian",
        "fedora": "fedora",
        "uio managed": "cloud-user",
        "rocky": "rocky",
        "ubuntu": "ubuntu"
    }

    while True:     # Tries to fetch the servers IPv6 address until it becomes available
        server = conn.get_server_by_id(server_id)
        if "IPv6" in server["addresses"]:   # IPv6 address will not be available right after server creation
            address = server["addresses"]["IPv6"][1]["addr"]
            break
        sleep(1)

    image_id = server["image"]["id"]
    image_name = conn.get_image_by_id(image_id)["name"]
    login_name = get_by_partial_name(user_names, image_name)    # Gets login name from user_names

    return login_name, address


def save_login_info(server, ssh_command: str) -> None:
    with open("nrec_login_info.csv", "a") as f:  # Writes ssh commands to a file in addition to printing
        fieldnames = ["server_id", "server_name", "ssh_command", "key_name"]
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        if f.tell() == 0:  # Write header if file is empty
            writer.writeheader()
        writer.writerow({
            "server_id": server["id"],
            "server_name": server["name"],
            "ssh_command": ssh_command,
            "key_name": server["key_name"]
        })


def main() -> None:
    args = get_arguments()
    university_username, nrec_username, project_name, password, region = args[:5]
    set_env(nrec_username, project_name, password, region)   # Environment variables are used by openstack
    conn = openstack.connect()
    try:
        conn.identity  # An unused request to conn which will only work if user credentials are correct
    except keystoneauth1.exceptions.http.Unauthorized:
        print("The login information you have entered is incorrect. ")
        exit()
    key, key_passphrase, server_name, source, flavor, list_sources, list_flavors = args[5:]
    if list_options(conn, list_sources, list_flavors):  # Lists sources and/or flavors if corresponding flags are set
        exit()                                          # If sources and/or flavors are listed, exit program
    print("Creating keypair")
    create_keypair(conn, key, key_passphrase)                           # A keypair is needed for ssh
    print("Creating security group")
    create_security_group(conn, region)                 # Allows ssh and icmp to the servers ip address
    print("Creating server")
    server = conn.create_server(
        server_name,
        image=source,
        flavor=flavor,
        security_groups=[f"ICMP_and_SSH_{region}", "default"],
        key_name=key,
        nics=[{"net-name": "IPv6"}]
    )
    print("Server created, login details will be available shortly")
    username, ipv6 = get_login_info(conn, server["id"])     # Constructs ssh commands to connect with
    create_ssh_config(ipv6, university_username, key)
    print(f"    Paste this in the terminal to log into server: ssh {username}@{ipv6}")
    save_login_info(server, f"ssh {username}@{ipv6}")       # Saves remotessh and localssh in a file


if __name__ == '__main__':
    main()
