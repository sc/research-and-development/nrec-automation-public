# NREC Automation

## Quick setup of your first instance

1. Create an NREC user
   
   This is well documented in the [NREC docs](https://docs.nrec.no/login.html).  
   If you already have a user, but have forgotten the NREC API key, follow the link until you see "Reset API password" and click it.  
   Also take note of your project name, it can be found in the top left corner of the NREC-dashboard.  

2. Clone this repository

   ```shell
   git clone https://git.app.uib.no/sc/research-and-development/nrec-automation-public.git
   ```

3. Change directory to the newly cloned repository

   ```shell
   cd nrec-automation-public
   ```

   All subsequent steps will be performed from this directory.

4. Make sure at **least Python 3.9.2** is installed, older versions will not work

   Your python version can be checked using following commands:

   ```shell
   python --version
   ```
   
   ```shell
   python3 --version
   ```

   If necessary, download the most recent [python version](https://www.python.org/downloads/).

5. Download and install necessary dependencies

   ```shell
   pip3 install -r requirements.txt
   ```

6. Run automation.py and fill in the required information in quotation marks  

   Flags with default values \[in brackets\] can be omitted if the default value is desirable. The first 5 options are  
   used for authentication and only have to be provided the first time running the script, they will be the default  
   values unless their respective flags are set again. Do not include the comments at the end of lines:

   ```shell
   python3 automation.py 
   -uu "UNIVERSITY_USERNAME" 
   -nu "NREC_USERNAME" 
   -pn "PROJECT_NAME" 
   -a "API_KEY" # Provided at first login to NREC
   -r "REGION" # "bgo" for Bergen, "osl" for Oslo
   -k "KEYNAME" # Choose any key name [nrec]
   -kp "PASSPHRASE" # Choose a passphrase for the key if creating a new one [no passphrase]
   -sn "SERVER_NAME" # Choose any server name [myserver]
   -s "'SOURCE'" # OS of the server, get available by --list_sources flag ['GOLD Ubuntu 22.04 LTS']
   -f "FLAVOR" # Resources allocated to the server, get available by --list_flavors flag [m1.small]
   ```

   First time running script example: 
   ```shell
   python3 automation.py -uu onr010 -nu ola.nrecuser@uib.no -pn DEMO-ola.nrecuser.uib.no -a apikey -r bgo
   ```
   
   Second time running only the script will yield the same result as above:
   ```shell
   python3 automation.py
   ```
   
   Listing sources and flavors:
   ```shell
   python3 automation.py --list_sources --list_flavors
   ```
   
   Creating Centos server with bigger quota:
   ```shell
   python3 automation.py -s 'GOLD CentOS Stream 9' -f m1.large
   ```
   
   Alternatively you can use the GUI by typing `python3 automation_gui.py`, leave the "Key Passphrase" field blank 
   if you are using an existing key or do not want a key passphrase in your new key.


7. The script should output an ssh command  

   The ssh command can be pasted into the terminal to access the server, it will look something like:

   ```shell
   ssh xxxxxx@2001:700:x:xxxx::xxxx
   ```

   When connecting to the server, your university password will be requested along with 2-factor authentication. 
   For mac and linux users you only have to do this at the start of a session, after logging out you will have 10 minutes where you can log in 
   without having to do this again.

   The commands for connecting to servers are also stored in a file called `nrec_login_info.csv`.

## Requesting more resources
Users will be allocated a demo project on their first login. This project is very resource limited and 
unreliable, and it is advised to apply for a standard project. This can be done by following this link:
https://request.nrec.no/


## Installing software

1. To get the software installers on the server run the ssh command with " < get_installers.sh" appended to it in the terminal (not while connected via ssh):

   ```shell
   ssh xxxxxx@2001:700:x:xxxx::xxxx < get_installers.sh
   ```

2. Now you can ssh into the server and run the installation scripts located there: 
   
   <b>Anaconda & Miniconda: </b> Run the installer for one of these, Anaconda for all the packages 
   (may take several minutes to download), Miniconda for a minimal installation with only Python and Jupyter Notebook:

   ```shell
   ./installanaconda.sh
   ./installminiconda.sh
   ```

   Close the ssh connection and re-connect for the installation to take effect. From now on you will be in the Conda 
   environment by default when connecting to the server. To exit it type `conda deactivate` and `conda avtivate` to 
   enter it again.

   <b>Jupyter Notebook: </b> This requires you to have ran one of the above conda installation scripts and be in the resulting environment. 
   Simply run `jupyter notebook` and paste one of the resulting links in your browser 
   (the file path option will not work, use one of the last 2 links below).
   The result should look something like this:
   ```
   To access the notebook, open this file in a browser:
         file:///home/ubuntu/.local/share/jupyter/runtime/nbserver-1602-open.html
   Or copy and paste one of these URLs:
         http://localhost:8888/?token=f281562aa4428400d82684f7d4cca664e658624f362ca9b9
      or http://127.0.0.1:8888/?token=f281562aa4428400d82684f7d4cca664e658624f362ca9b9
   ```
   **RStudio:** This requires you to have ran one of the above conda installation scripts and be in the resulting 
   environment. To get RStudio run `./installr.sh`. To test if R is installed type `R` when logged into the server, 
   and you should be able to use it interactively. RStudio can now be used in Jupyter Notebook as well.  
   **Note:** If you have a demo project the biggest flavor available is m1.small, which does not have the disk space 
   for installing R in addition to the full Anaconda package.
   
   **Julia:** This requires you to have ran one of the above conda installation scripts and be in the resulting 
   environment. To get Julia run `./installjulia.sh`. To test if Julia is installed type `julia` when logged into the 
   server, and you should be able to use it interactively. Julia can now be used in Jupyter Notebook as well.

   **EESSI:** To install EESSI run `./installeessi.sh`.  
   Type `source /cvmfs/pilot.eessi-hpc.org/latest/init/bash` to use the EESSI environment.
      

## Features

### Key generation
This application will create a keypair with the name the user specified. 
In the case only parts of the key is defined this will happen:  
-If this keypair already exists locally in the folder ~/.ssh and the key is uploaded to NREC no action will be taken.  
-If the keypair only exists locally no new keys will be made, but the existing key will be uploaded to NREC.  
-If the key is only uploaded to NREC, a new keypair will be created, and overwrite the one uploaded to NREC.  

When these comparisons are made, key names are only considered, trying to use an existing key with a new passphrase will
not change the passphrase

If you use a passphrase it can be cumbersome to have to enter it every time entering the server. It is recommended to 
use the `ssh-add <keyname>` command, so you only have to enter the passphrase at the start of a session. 

**MacOS & Linux**

```shell
# Start the ssh agent if it is not started yet: 
eval `ssh-agent`
# Now add the key, replace "key" with the appropriate key name
ssh-add ~/.ssh/"key"
```

**Windows**  
Open powershell as administrator and enable the ssh-agent service: 

```shell
Get-Service -Name ssh-agent | Set-Service -StartupType Automatic
Start-Service ssh-agent
```

Now you can use ssh-add from any shell: 

```shell
# Change "home-directory" to your home directory, and "key" to appropriate key
ssh-add "home-directory"/.ssh/"key"
```

The key is not visible from the NREC dashboard, but it does exist and can be displayed with Openstack.

### Security Group Generation
Security groups are important in order to allow remote access to the server. The application will create one automatically for the user.
It will either be called **ICMP_and_SSH_bgo** or **ICMP_and_SSH_osl** depending on what university network the user uses. 
If there already exists a security group with such a name no new one will be made and the already existing one will be used by the server (even if the rules within the group are wrong).

### Server generation
Using the features above and the users choice of source and flavor, 
the application will create a server for the user which can be reached with an IPv6 address.

### SSH config file
An ssh config file is created automatically, making sure that:  
-The appropriate key is automatically used  
-Connections happens through the universities' login host  
-An ssh-tunnel is set up from port 8888 to port 8888 for jupyter notebook  

## Exceptions
Sometimes the program will stop running if something is wrong. 
This is intended behaviour and the application will let the user know what is wrong and how to fix it. <br> 
They can be caused by: <br>
-Wrong login credentials <br>
-Not enough allocated resources

## Server deletion
It is possible to delete the server from the CLI or a script using Openstack, but both of those options requires entering more 
credentials than just deleting the server from the NREC dashboard. 
To delete from the dashboard: Compute -> Instances -> Downwards facing arrow to the right of instance -> Delete Instance